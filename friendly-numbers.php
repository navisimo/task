<?php
//Задача. Дружественные и совершенные числа.
//1. функция определяет, что два числа являются дружественными.

function compare($num1, $num2)
{
    $sumA = 0;
    for ($i = 1; $i < $num1; $i++) {
        if( ($num1%$i) == 0) {
            $sumA = $sumA + $i;
        }
    }

    $sumB = 0;
    for ($j = 1; $j < $num2; $j++) {
        if(($num2%$j) == 0) {
            $sumB = $sumB + $j;
        }
    }

    if($num1 == $sumB && $num2 == $sumA) echo $num1 . ' и ' . $num2 .' - являются дружественными числами! т.к. <br> ';
    else echo $num1 . ' и ' . $num2 .' - не являются дружественными числами! т.к. <br> ';

    echo $num1 . ': sum - ' . $sumA . '<br>' . $num2 . ': sum - ' . $sumB . '<br>';

}
compare(12285, 14595);
compare(947835,  1125765);
compare(998104, 1043096);
compare(2312321, 291967);

