<?php
//Задача.
//функция определяет, что переданное число является совершенным.
function Number($num)
{
    $sumA = 0;
    for ($i = 1; $i < $num; $i++) {
        if( ($num%$i) == 0) {
            $sumA = $sumA + $i;
        }
    }

    if($num == $sumA) {
        echo $num . ' - совершенное число!';
    } else {
        echo $num . ' - не совершенное число!';
    }
}

Number(28);
Number(8128);
Number(8228);
Number(10);


